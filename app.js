/*
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const path = require("path");
*/

import express from "express";
import morgan from "morgan";
import cors from "cors";
import path from "path";
import sequelize  from "./database/db";


const app = express();

app.use(cors());
app.use(morgan("tiny"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.use('/api', require('./routes/usuario'));
app.use('/api', require('./routes/perfil'));
app.use('/api', require('./routes/empresa'));
app.use('/api', require('./routes/zona'));
app.use('/api', require('./routes/producto'));
app.use('/api', require('./routes/tipoestado'));
app.use('/api', require('./routes/envio'));
app.use('/api', require('./routes/logenvio'));
app.use('/api', require('./routes/comuna'));
app.use('/api', require('./routes/enviodetalle'));
app.get("/", (req, res) =>{
     res.send("Hola Mundo");
});

app.set('port',process.env.PORT || 5000 );
app.listen(app.get("port"),()=>{

    console.log("Server en el puerto",app.get("port"));

    sequelize.sync({force: false}).then(()=>{
         console.log("Conectado a la base de datos");
  
          const comunas = [{}]
    }).catch(error =>{
         console.log("se ha producido un error", error);
    })
    
})