import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Producto from "./Producto";
import Envio from "./Envio";

class EnvioDetalle extends Model {}
EnvioDetalle.init({
    id_envio_detalle: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  cantidad: DataTypes.INTEGER,
  monto:  DataTypes.INTEGER,
  

}, { sequelize, modelName: 'enviodetalle' });


EnvioDetalle.belongsTo(Producto, {as: 'Producto', foreignKey: 'id_producto'});
EnvioDetalle.belongsTo(Envio, {as: 'Envio', foreignKey: 'id_envio'});
module.exports = EnvioDetalle;