import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Comuna from "./Comuna";

class Empresa extends Model {}
Empresa.init({
    id_empresa: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  nombre: DataTypes.STRING,
  rut: DataTypes.STRING,
  giro:  DataTypes.STRING,
  direccion:  DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'empresa' });

Empresa.belongsTo(Comuna, {as: 'Comuna', foreignKey: 'id_comuna'});
module.exports = Empresa;