import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Usuario from "./Usuario";

class Perfil extends Model {}
Perfil.init({
    id_perfil: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  perfil: DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'perfil' });


module.exports = Perfil;