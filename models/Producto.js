import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Empresa from "./Empresa";
import Usuario from "./Zona";

class Producto extends Model {}
Producto.init({
    id_producto: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  producto: DataTypes.STRING,
  codigo: DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'producto' });


Producto.belongsTo(Empresa, {as: 'Empresa', foreignKey: 'id_empresa'});
Producto.belongsTo(Usuario, {as: 'Usuario', foreignKey: 'id_usuario'});
module.exports = Producto;