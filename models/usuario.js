import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Perfil from "./Perfil";
import Empresa from "./Empresa";
import Zona from "./Zona";

class Usuario extends Model {}
Usuario.init({
    id_usuario: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  username: DataTypes.STRING,
  password: DataTypes.STRING,
  correo:  DataTypes.STRING,
  nombre:  DataTypes.STRING,
  apellido: DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'usuario' });

Usuario.belongsToMany(Zona, { through: "usuario_zona" }); 
Usuario.belongsTo(Perfil, {as: 'Perfil', foreignKey: 'id_perfil'});
Usuario.belongsTo(Empresa, {as: 'Empresa', foreignKey: 'id_empresa'});
module.exports = Usuario;