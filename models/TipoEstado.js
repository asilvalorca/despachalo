import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";


class TipoEstado extends Model {}
TipoEstado.init({
    id_tipo_estado: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  
  tipoestado: DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'tipoestado' });

module.exports = TipoEstado;