import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Comuna  from "./Comuna";
import Usuario  from "./Usuario";

class Zona extends Model {}
Zona.init({
    id_zona: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  zona: DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'zona' ,  underscored: true});

//Zona.belongsToMany(Usuario, { through: "zona_comuna" });
Zona.belongsToMany(Comuna, { through: "zona_comuna" });
module.exports = Zona;