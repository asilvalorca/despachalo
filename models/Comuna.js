import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";

class Comuna extends Model {}
Comuna.init({
    id_comuna: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  comuna: DataTypes.STRING,
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'comuna' });


module.exports = Comuna;