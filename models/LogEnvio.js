import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Envio  from "./Envio";
import TipoEstado  from "./TipoEstado";

class LogEnvio extends Model {}
LogEnvio.init({
    id_log_envio: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  estado: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
 
}, { sequelize, modelName: 'logenvio' });

//LogEnvio.belongsToMany(Usuario, { through: "log_envio_comuna" });

LogEnvio.belongsTo(Envio, {as: 'Envio', foreignKey: 'id_envio'});
LogEnvio.belongsTo(TipoEstado, {as: 'TipoEstado', foreignKey: 'id_tipo_envio'});
module.exports = LogEnvio;