import  { Sequelize, Model, DataTypes } from 'sequelize';
import sequelize from "../database/db";
import Empresa from "./Empresa";
import Usuario from "./Usuario";
import Comuna from "./Comuna";
import TipoEstado from "./TipoEstado";

class Envio extends Model {}
Envio.init({
    id_envio: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
  },
  envio: DataTypes.STRING,
  monto:  DataTypes.INTEGER,
  nombre_destinatario: DataTypes.STRING,
  rut: DataTypes.STRING,
  telefono: DataTypes.INTEGER,
  direccion: DataTypes.STRING,
  numero: DataTypes.INTEGER,
  correo: DataTypes.STRING,
  comentario: DataTypes.TEXT,
  fecha_compra: DataTypes.DATEONLY,
  fecha_entrega: DataTypes.DATEONLY,

}, { sequelize, modelName: 'envio' });


Envio.belongsTo(Empresa, {as: 'Empresa', foreignKey: 'id_empresa'});
Envio.belongsTo(Usuario, {as: 'Usuario', foreignKey: 'id_usuario'});
Envio.belongsTo(Usuario, {as: 'UsuarioConductor', foreignKey: 'id_conductor'});
Envio.belongsTo(Comuna, {as: 'Comuna', foreignKey: 'id_comuna'});
Envio.belongsTo(TipoEstado, {as: 'TipoEstado', foreignKey: 'id_tipo_estado'});
module.exports = Envio;