import multer from "multer"; 
import path from "path"; 


const storage = multer.diskStorage({
    destination: "./archivos",
    filename: (req, file, cb) =>{
        cb("",Date.now()+path.extname(file.originalname));
    }
});

const upload = multer({
   storage,
   limits:{  fileSize: 1000000  },
  

})

module.exports = upload;