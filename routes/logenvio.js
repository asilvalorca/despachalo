import express from "express";
import bcrypt  from 'bcrypt';
import LogEnvio from '../models/LogEnvio';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

const router = express.Router();


//*********************** * Crear Perfil *************************
router.post("/nuevo-perfil",[
    check('perfil',"El perfil  es obligatorio").not().isEmpty()
   
],async(req, res)=>{

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
        perfil: req.body.perfil,      
    };
   
   
    const buscarPerfil = await Perfil.findAll({
        where: {        
                 perfil: req.body.perfil 
        }
      });

      if(buscarPerfil.length > 0){
        res.status(200).json({mensaje: "El perfil ya existe", estado: 0, errores: ""});
        next();
      }
    
    try{
        const perfilDB = await Perfil.create(body);
        //res.status(200).json(usuarioDB);
        res.status(200).json({mensaje: "Perfil guardado con exito", estado: 1, errores: ""});

    }catch (error){
        return res.status(500).json({
            mensaje: "ocurrio un error",
            error,
            estado: 0
        })
    }
});


//*********************** * listar Perfiles *************************
router.get("/listar-perfiles",async(req, res)=>{

    const perfilDB = await Perfil.findAll();
   // console.log("All perfiles:", JSON.stringify(perfiloDB, null, 2));
    res.status(200).json(perfilDB);
    
});

//*********************** * listar perfiles  activos*************************
router.get("/listar-perfiles-activos",async(req, res)=>{

    const perfilDB = await Perfil.findAll({
        where: {        
                 estado: 1 
        }
      });
    console.log("All perfiles:", JSON.stringify(perfilDB, null, 2));
    res.status(200).json(perfilDB);
    
});


//*********************** * Actualizar perfil*************************
router.put("/perfil/:id",[
    check('perfil',"El perfil  es obligatorio").not().isEmpty(),
],async(req, res)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
       
        perfil: req.body.perfil,
           
    };
      
    const buscarPerfil = await Usuario.findAll({
        where: {
            perfil: req.body.perfil
        }
      });
      if(buscarPerfil.length > 0){
        const id_perfil = buscarPerfil[0].dataValues.id_perfil;
        if(id_perfil != _id){
         res.status(200).json({mensaje: "El perfil ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    try{
        const perfilDB = await Perfil.update(body,{
            where: {
                id_perfil: _id
              }
        });
        if(!perfilDB){
            res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
        }else{
            res.status(200).json({mensaje: "Perfil editado con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});
module.exports = router;