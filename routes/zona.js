import express from "express";
import bcrypt  from 'bcrypt';
import Zona from '../models/Zona';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

const router = express.Router();


//*********************** * Crear Zona *************************
router.post("/nueva-zona",[
    check('zona',"El nombre de la zona  es obligatorio").not().isEmpty()
   
],async(req, res)=>{

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
        zona: req.body.zona,      
    };
   
   
    const buscarZona = await Zona.findAll({
        where: {        
                 zona: req.body.zona 
        }
      });

      if(buscarZona.length > 0){
        res.status(200).json({mensaje: "El zona ya existe", estado: 0, errores: ""});
        next();
      }
    
    try{
        const zonaDB = await Zona.create(body);
        //res.status(200).json(usuarioDB);
        res.status(200).json({mensaje: "Zona guardado con exito", estado: 1, errores: ""});

    }catch (error){
        return res.status(500).json({
            mensaje: "ocurrio un error",
            error,
            estado: 0
        })
    }
});


//*********************** * listar Zonaes *************************
router.get("/listar-zonas",async(req, res)=>{

    const zonaDB = await Zona.findAll();
   // console.log("All zonaes:", JSON.stringify(zonaoDB, null, 2));
    res.status(200).json(zonaDB);
    
});

//*********************** * listar zonaes  activos*************************
router.get("/listar-zonas-activos",async(req, res)=>{

    const zonaDB = await Zona.findAll({
        where: {        
                 estado: 1 
        }
      });
    console.log("All zonas:", JSON.stringify(zonaDB, null, 2));
    res.status(200).json(zonaDB);
    
});


//*********************** * Actualizar zona*************************
router.put("/zona/:id",[
    check('zona',"El zona  es obligatorio").not().isEmpty(),
],async(req, res)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
       
        zona: req.body.zona,
           
    };
      
    const buscarZona = await Zona.findAll({
        where: {
            zona: req.body.zona
        }
      });
      if(buscarZona.length > 0){
        const id_zona = buscarZona[0].dataValues.id_zona;
        if(id_zona != _id){
         res.status(200).json({mensaje: "El zona ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    try{
        const zonaDB = await Zona.update(body,{
            where: {
                id_zona: _id
              }
        });
        if(!zonaDB){
            res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
        }else{
            res.status(200).json({mensaje: "Zona editada con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});
module.exports = router;