import express from "express";
import bcrypt  from 'bcrypt';
import Producto from '../models/Producto';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";
import xlsx from "xlsx"; 
import upload from "../middlewares/upload";
import nodemailer from "nodemailer";

const router = express.Router();


//*********************** * Crear Producto *************************
router.post("/nuevo-producto",[
    check('producto',"El producto  es obligatorio").not().isEmpty(),
    check('id_empresa',"La empresa  es obligatoria").not().isEmpty().isInt(),
    check('id_usuario',"El usuario  es obligatoria").not().isEmpty().isInt()
],async(req, res)=>{

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
        producto: req.body.producto,  
        codigo: req.body.codigo,
        id_empresa: req.body.id_empresa,
        id_usuario: req.body.id_usuario    
    };
   
   
    const buscarProducto = await Producto.findAll({
        where: {    
                [Op.or]: [
                    { producto: req.body.producto },
                    { codigo: req.body.codigo }
                ],    
                 
                 id_empresa: req.body.id_empresa,            
        }
      });

      if(buscarProducto.length > 0){
        res.status(200).json({mensaje: "El producto ya existe", estado: 0, errores: ""});
        next();
      }
    
    try{
        const productoDB = await Producto.create(body);
        //res.status(200).json(usuarioDB);
        res.status(200).json({mensaje: "Producto guardado con exito", estado: 1, errores: ""});

    }catch (error){
        return res.status(500).json({
            mensaje: "ocurrio un error",
            error,
            estado: 0
        })
    }
});


//*********************** * listar Productos *************************
router.get("/listar-productos/:empresa?",async(req, res)=>{
   /* const empresa = req.params.empresa;
    const condicion = {};
    if(empresa != undefined || empresa !=''){
        condicion = {
            estado : 1,
            id_empresa : 1
        }
    }else{
        condicion = {
            estado : 1,
           
        }
    }*/
    const productoDB = await Producto.findAll();
   // console.log("All productoes:", JSON.stringify(productooDB, null, 2));
    res.status(200).json(productoDB);
    
});

//*********************** * listar Productos  activos*************************
router.get("/listar-productos-activos/:empresa?",async(req, res)=>{
   /* const empresa = req.params.empresa;
    const condicion = {};
    if(empresa != undefined || empresa !=''){
        condicion = {where:{
          
            id_empresa : 1
        }}
    }else{
        condicion = "";
    }
    */
    const productoDB = await Producto.findAll();
    console.log("All productoes:", JSON.stringify(productoDB, null, 2));
    res.status(200).json(productoDB);
    
});


//*********************** * Actualizar producto*************************
router.put("/producto/:id",[
    check('producto',"El producto  es obligatorio").not().isEmpty(),
],async(req, res)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
       
        producto: req.body.producto,  
        codigo: req.body.codigo,
        id_empresa: req.body.id_empresa,
        id_usuario: req.body.id_usuario   
           
    };
      
    const buscarProducto = await Producto.findAll({
        where: {    
            [Op.or]: [
                { producto: req.body.producto },
                { codigo: req.body.codigo }
            ],    
             
             id_empresa: req.body.id_empresa,            
    }
      });
      if(buscarProducto.length > 0){
        const id_producto = buscarProducto[0].dataValues.id_producto;
        if(id_producto != _id){
         res.status(200).json({mensaje: "El producto ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    try{
        const productoDB = await Producto.update(body,{
            where: {
                id_producto: _id
              }
        });
        if(!productoDB){
            return res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
        }else{
           return res.status(200).json({mensaje: "Producto editado con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});

router.get("/leer-excel",async(req, res)=>{
    const workbook = xlsx.readFile("archivos/prueba.xlsx");
    const workbookSheets = workbook.SheetNames;
    const sheet = workbookSheets[0];
    const dataExcel = xlsx.utils.sheet_to_json(workbook.Sheets[sheet]);
    console.log(dataExcel);
});

router.post("/subir-archivo",upload.single('file'),async(req, res)=>{
   const nombreArchivo = req.file.filename;

    const workbook = xlsx.readFile("archivos/"+nombreArchivo);
    const workbookSheets = workbook.SheetNames;
    const sheet = workbookSheets[0];
    const dataExcel = xlsx.utils.sheet_to_json(workbook.Sheets[sheet]);
    console.log(dataExcel);
    return res.status(200).json({mensaje: "archivo "+nombreArchivo+" Subido con exito", estado: 1, errores: ""});
});

router.get("/correo",async(req, res)=>{
  
 
    async function main() {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        let testAccount = await nodemailer.createTestAccount();
      
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
          host: "mail.uman.cl",
          port: 25,
          secure: false, // true for 465, false for other ports
          auth: {
            user: 'veladero@uman.cl', // generated ethereal user
            pass: 'veladero2018', // generated ethereal password
          },
          tls:{rejectUnauthorized: false}
        });
      
        // send mail with defined transport object
        let info = await transporter.sendMail({
          from: '"Despachalo "sistema.compras@sistemas.bailac.cl', // sender address
          to: "a.silvalorca@gmail.com", // list of receivers
          subject: "Hello ✔", // Subject line
          text: "Hello world?", // plain text body
          html: "<b>Hello world?</b>", // html body
        });
      
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
      
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      }
      
      main().catch(console.error);
 });
module.exports = router;