import express from "express";
import bcrypt  from 'bcrypt';
import Comuna from '../models/Comuna';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

const router = express.Router();


//*********************** * Crear empresas *************************
router.post("/nueva-comuna",[
    check('comuna',"El nombre de la comuna es obligatorio").not().isEmpty()
   
],async(req, res)=>{

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
        comuna: req.body.comuna,      
    };
   
   
    const buscarComuna = await Comuna.findAll({
        where: {        
                 comuna: req.body.comuna 
        }
      });

      if(buscarComuna.length > 0){
        res.status(200).json({mensaje: "El comuna ya existe", estado: 0, errores: ""});
        next();
      }
    
    try{
        const comunaDB = await Comuna.create(body);
        //res.status(200).json(usuarioDB);
        res.status(200).json({mensaje: "Comuna guardada con exito", estado: 1, errores: ""});

    }catch (error){
        return res.status(500).json({
            mensaje: "ocurrio un error",
            error,
            estado: 0
        })
    }
});


//*********************** * listar Comunaes *************************
router.get("/listar-comunas",async(req, res)=>{

    const comunaDB = await Comuna.findAll();
   // console.log("All comunaes:", JSON.stringify(comunaoDB, null, 2));
    res.status(200).json(comunaDB);
    
});

//*********************** * listar comunaes  activos*************************
router.get("/listar-comunas-activas",async(req, res)=>{

    const comunaDB = await Comuna.findAll({
        where: {        
                 estado: 1 
        }
      });
    console.log("All comunaes:", JSON.stringify(comunaDB, null, 2));
    res.status(200).json(comunaDB);
    
});


//*********************** * Actualizar comuna*************************
router.put("/comuna/:id",[
    check('comuna',"El comuna  es obligatorio").not().isEmpty(),
],async(req, res)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
       
        comuna: req.body.comuna,
           
    };
      
    const buscarComuna = await Comuna.findAll({
        where: {
            comuna: req.body.comuna
        }
      });
      if(buscarComuna.length > 0){
        const id_comuna = buscarComuna[0].dataValues.id_comuna;
        if(id_comuna != _id){
         res.status(200).json({mensaje: "El comuna ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    try{
        const comunaDB = await Comuna.update(body,{
            where: {
                id_comuna: _id
              }
        });
        if(!comunaDB){
            res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
        }else{
            res.status(200).json({mensaje: "Comuna editada con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});

module.exports = router;