import express from "express";
import bcrypt  from 'bcrypt';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";
import Usuario from '../models/Usuario';
import Empresa from '../models/Empresa';
import Comuna from '../models/Comuna';
import Envio from '../models/Envio';
const router = express.Router();
const saltRounds = 10;


//*********************** * Crear envio *************************
router.post("/nuevo-envio",[
    check('id_empresa',"La empresa es obligatoria").not().isEmpty(),
    check('id_comuna',"La comuna es obligatoria").not().isEmpty(),
    check('monto',"El monto  es obligatoria").isInt(),
    check('nombre_destinatario',"El nombre del destinario es obligatorio").not().isEmpty(),
    check('rut',"El rut del destinario es obligatorio").not().isEmpty(),
    check('telefono',"La teléfono es obligatoria").not().isEmpty(),
    check('numero',"El Número es obligatoria").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),
    check('id_conductor',"El perfil  debe estar correcto").isInt(),
    check('fecha_compra',"La Fecha de compras  es obligatorio").isDate(),
    check('direccion',"La direccioón es obligatoria").not().isEmpty(),
    check('id_usuario',"El usuario es obligatoria").not().isEmpty(),
    check('fecha_entrega',"La Fecha de entrega de la compras es obligatorio").isDate(),

],async(req, res)=>{

    console.log("nuevo envio");

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }   
    const body = {
        id_empresa: req.body.id_empresa,
        id_comuna: req.body.id_comuna,
        numero: req.body.numero,
        correo: req.body.correo,
        rut: req.body.rut,    
        monto: req.body.monto,  
        telefono: req.body.telefono,  
        id_conductor: req.body.id_conductor,  
        direccion: req.body.direccion,
        id_usuario: req.body.id_usuario,
        fecha_compra: req.body.fecha_compra,  
        fecha_entrega: req.body.fecha_entrega,  
      
    };
   
    
    
    const buscarEnvio = await Envio.findAll({
        where: {
            [Op.and]: [
                { numero: req.body.numero },
                { id_empresa: req.body.id_empresa }
              ]
        }
      });
      if(buscarEnvio.length > 0){
        res.status(403).json({mensaje: "El envio ya existe", estado: 0, errores: ""});
        next();
      }
    
    try{
        const envioDB = await Envio.create(body);
        //res.status(200).json(envioDB);
        res.status(200).json({mensaje: "Envio guardado con exito", estado: 1, errores: ""});
    }catch (error){
        return res.status(500).json({
            mensaje: "ocurrio un error",
            error,
            estado: 0
        })
    }
});

//*********************** * listar envios *************************
router.get("/listar-envios",async(req, res)=>{

    const envioDB = await Envio.findAll({ include:[{
            model: Perfil,
            as: "Perfil",
            attributes: ['perfil']
        },
        {
            model: Empresa,
            as: "Empresa",
            attributes: ['nombre'],
            required:false
        },{
            model: Zona,
            attributes: ['zona'],
            required:false
        }]
    });
    console.log("All users:", JSON.stringify(envioDB, null, 2));
    res.status(200).json(envioDB);
    
});


//*********************** * Actualizar Envios*************************
router.put("/envio/:id",[
    check('username',"El nombre de envio es obligatorio").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),
    check('id_perfil',"El perfil  debe estar correcto").isInt(),
    check('nombre',"El nombre  es obligatorio").not().isEmpty(),
    check('apellido',"El apellido  es obligatorio").not().isEmpty(),
    check('estado',"El estado  es obligatorio").isInt(),

],async(req, res)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }

    const buscarEnvio = await Envio.findAll({
        where: {
            [Op.or]: [
                { username: req.body.username },
                { correo: req.body.correo }
              ]
        }
      });
      if(buscarEnvio.length > 0){
        const id_envio = buscarEnvio[0].dataValues.id_envio;
        if(id_envio != _id){
         res.status(200).json({mensaje: "El envio ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    const body = {
        username: req.body.username,
        id_perfil: req.body.id_perfil,
        nombre: req.body.nombre,
        correo: req.body.correo,
        apellido: req.body.apellido,    
        estado: req.body.estado,    
      
    };

   if(body.password){
    body.password = bcrypt.hashSync(req.body.password, saltRounds);
   }
   
    
    
    try{
        const envioDB = await Envio.update(body,{
            where: {
                id_envio: _id
              }
        });
        if(!envioDB){
            res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
        }else{
            res.status(200).json({mensaje: "Envio actualizado con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});

router.get("/crea-envios2",async(req, res)=>{

    
const user = await Envio.create({
   nombre: 'andres',
   
});

const zonaDB = await Zona.findAll();

//const zonaDB = await Zona.create({zona: "zona sur"});

console.log(zonaDB);


user.setZonas([zonaDB.json()])
});
module.exports = router;