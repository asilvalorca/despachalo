import express from "express";
import bcrypt  from 'bcrypt';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";
import Usuario from '../models/Usuario';
import Perfil from '../models/Perfil';
import Empresa from '../models/Empresa';
import Zona from '../models/Zona';
import jwt from "jsonwebtoken";
import {verificarAuth, verificarAdministrador}  from "./../middlewares/autenticacion";


const router = express.Router();
const saltRounds = 10;


//*********************** * Crear usuario *************************
router.post("/nuevo-usuario",[
    check('username',"El nombre de usuario es obligatorio").not().isEmpty(),
    check('password',"El password  es obligatorio").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),
    check('id_perfil',"El perfil  debe estar correcto").isInt(),
    check('nombre',"El nombre  es obligatorio").not().isEmpty(),
    check('apellido',"El apellido  es obligatorio").not().isEmpty(),
],async(req, res)=>{

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
        username: req.body.username,
        id_perfil: req.body.id_perfil,
        nombre: req.body.nombre,
        correo: req.body.correo,
        apellido: req.body.apellido,    
      
    };
   
    body.password = bcrypt.hashSync(req.body.password, saltRounds);
    
    const buscarUsuario = await Usuario.findAll({
        where: {
            [Op.or]: [
                { username: req.body.username },
                { correo: req.body.correo }
              ]
        }
      });
      if(!buscarUsuario){
        return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
    }
      if(buscarUsuario.length > 0){
       return res.status(403).json({mensaje: "El usuario ya existe", estado: 0, errores: ""});
       
      }
    
    try{
        const usuarioDB = await Usuario.create(body);
        //res.status(200).json(usuarioDB);
        res.status(200).json({mensaje: "Usuario guardado con exito", estado: 1, errores: ""});
    }catch (error){
        return res.status(500).json({
            mensaje: "ocurrio un error",
            error,
            estado: 0
        })
    }
});

//*********************** * listar usuarios *************************
router.get("/listar-usuarios",async(req, res)=>{

    const usuarioDB = await Usuario.findAll({ include:[{
            model: Perfil,
            as: "Perfil",
            attributes: ['perfil']
        },
        {
            model: Empresa,
            as: "Empresa",
            attributes: ['nombre'],
            required:false
        },{
            model: Zona,
            attributes: ['zona'],
            required:false
        }]
    });
    console.log("All users:", JSON.stringify(usuarioDB, null, 2));
    res.status(200).json(usuarioDB);
    
});


//*********************** * Actualizar Usuarios*************************
router.put("/usuario/:id",[
    check('username',"El nombre de usuario es obligatorio").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),
    check('id_perfil',"El perfil  debe estar correcto").isInt(),
    check('nombre',"El nombre  es obligatorio").not().isEmpty(),
    check('apellido',"El apellido  es obligatorio").not().isEmpty(),
    check('estado',"El estado  es obligatorio").isInt(),

],async(req, res)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }

    const buscarUsuario = await Usuario.findAll({
        where: {
            [Op.or]: [
                { username: req.body.username },
                { correo: req.body.correo }
              ]
        }
      });
      if(!buscarUsuario){
        return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
    }
      if(buscarUsuario.length > 0){
        const id_usuario = buscarUsuario[0].dataValues.id_usuario;
        if(id_usuario != _id){
         res.status(200).json({mensaje: "El usuario ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    const body = {
        username: req.body.username,
        id_perfil: req.body.id_perfil,
        nombre: req.body.nombre,
        correo: req.body.correo,
        apellido: req.body.apellido,    
        estado: req.body.estado,    
      
    };

    if(body.password){
        body.password = bcrypt.hashSync(req.body.password, saltRounds);
    }
   
    
    
    try{
        const usuarioDB = await Usuario.update(body,{
            where: {
                id_usuario: _id
              }
        });
        if(!usuarioDB){
            res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
        }else{
            res.status(200).json({mensaje: "Usuario actualizado con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});

router.get("/crea-usuarios2",async(req, res)=>{

    
const user = await Usuario.create({
   nombre: 'andres',
   
});

const zonaDB = await Zona.findOne({ where: { id_zona: 1 } });

//const zonaDB = await Zona.create({zona: "zona sur"});

console.log(zonaDB);


user.setZonas(zonaDB);
});




router.post("/login",async(req, res)=>{

   
    const buscarUsuario = await Usuario.findOne({
        attributes: ['id_usuario', 'id_perfil', 'correo', 'nombre', 'apellido', "password"],
        where: {
            [Op.or]: [
                { username: req.body.username },
                { correo: req.body.correo }
              ],
             
        }
      });
    if(!buscarUsuario){
        return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
    }
    if(buscarUsuario.length == 0){
        return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
    }
    console.log( req.body.password);
    console.log( buscarUsuario.password);
    if(!bcrypt.compareSync( req.body.password, buscarUsuario.password)){
        return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta 2", estado: 0, errores: ""});   
    }
    
    buscarUsuario.password = "";
    const token =  jwt.sign({
        data:buscarUsuario
     },'secret',{expiresIn: 60 * 60 * 24 * 30});

    return res.json({buscarUsuario, token});
});




router.get("/leer-usuario",verificarAuth,async(req, res)=>{

    return  res.status(200).json({mensaje:'si entra'});
});

module.exports = router;