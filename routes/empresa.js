import express from "express";
import bcrypt  from 'bcrypt';
const router = express.Router();
import Empresa from '../models/Empresa';
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";



//*********************** * Crear empresas *************************
router.post("/nueva-empresa",[
    check('nombre',"El nombre de empresa es obligatorio").not().isEmpty(),
    check('giro',"El giro es obligatorio").not().isEmpty(),
    check('rut',"El rut es obligatorio").not().isEmpty(), 
   
],async(req, res, next)=>{

    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }

   if(req.body.id_comuna =='' || req.body.id_comuna ==0){
    req.body.id_comuna = null;
   }
    const body = {
        nombre: req.body.nombre,
        giro: req.body.giro,
        rut: req.body.rut,
        direccion: req.body.direccion,
        id_comuna: req.body.id_comuna,    
      
    };
   
   
    
    const buscarEmpresa = await Empresa.findAll({
        where: {
            [Op.or]: [
                { rut: req.body.rut },
               
              ]
        }
      });
      if(buscarEmpresa.length > 0){
        res.status(403).json({mensaje: "La empresa ya existe", estado: 0, errores: ""});
        return false;
        
      }else{

        
          try{
            const empresaDB = await Empresa.create(body);
            //res.status(200).json(empresaDB);
            res.status(200).json({mensaje: "Empresa guardado con exito", estado: 1, errores: ""});
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    }
});


//*********************** * listar empresa *************************
router.get("/listar-empresas",async(req, res)=>{

    const empresaDB = await Empresa.findAll();
    console.log("All users:", JSON.stringify(empresaDB, null, 2));
    res.status(200).json(empresaDB);
    
});



//*********************** * listar empresa activas*************************
router.get("/listar-empresas-activas",async(req, res)=>{

    const empresaDB = await Empresa.findAll({ 
        where: {
            estado: 1
        }
          }  );
    console.log("All users:", JSON.stringify(empresaDB, null, 2));
    res.status(200).json(empresaDB);
    
});


//*********************** * Actualizar empresa*************************
router.put("/empresa/:id",[
    check('nombre',"El nombre de empresa es obligatorio").not().isEmpty(),
    check('giro',"El giro es obligatorio").not().isEmpty(),
    check('rut',"El rut es obligatorio").not().isEmpty(), 

],async(req, res, next)=>{
    const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }

    const buscarEmpresa = await Empresa.findAll({
        where: {
            [Op.or]: [
                { rut: req.body.rut },
               
              ]
        }
      });
      if(buscarEmpresa.length > 0){
       const id_empresa = buscarEmpresa[0].dataValues.id_empresa;
       if(id_empresa != _id){
        res.status(200).json({mensaje: "El empresa ya existe", estado: 0, errores: ""});
        return false;
       }
        
      }
          

      if(req.body.id_comuna =='' || req.body.id_comuna ==0){
        req.body.id_comuna = null;
       }
        const body = {
            nombre: req.body.nombre,
            giro: req.body.giro,
            rut: req.body.rut,
            direccion: req.body.direccion,
            id_comuna: req.body.id_comuna,    
            estado: req.body.estado,
        };    
    
    try{
        const empresaDB = await Empresa.update(body,{
            where: {
                id_empresa: _id
              }
        });
        if(!empresaDB){
            res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
        }else{
            res.status(200).json({mensaje: "Empresa  actualizada con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
});

module.exports = router;