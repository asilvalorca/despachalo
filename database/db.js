import { Sequelize }  from 'sequelize';
import { databaseDev, databaseProduction } from "../config";

const sequelize = new Sequelize(
    databaseProduction.database,
    databaseProduction.username,
    databaseProduction.password,
    {
        host: databaseProduction.host,
        dialect: "mysql"
    }
);
module.exports = sequelize;